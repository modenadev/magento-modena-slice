<?php

namespace Webexpert\Slice\Controller\Response;

class Callback extends \Magento\Framework\App\Action\Action {

  protected $logger;

  protected $request;

  protected $orderCheck;

  protected $checkoutSession;

  protected $orderFactory;

  protected $urlRequest;

  public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Magento\Checkout\Model\Session       $session,
    \Magento\Sales\Model\OrderFactory     $orderFactory,
    \Psr\Log\LoggerInterface              $logger,
    \Webexpert\Slice\Model\Request        $request,
    \Webexpert\Slice\Model\OrderCheck     $orderCheck,
    \Magento\Framework\App\Request\Http   $urlRequest
  ) {
    parent::__construct($context);
    $this->logger = $logger;
    $this->request = $request;
    $this->orderCheck = $orderCheck;
    $this->checkoutSession = $session;
    $this->orderFactory = $orderFactory;
    $this->urlRequest = $urlRequest;
  }

  public function execute() {

    $params = $this->urlRequest->getParams();
    $order = $this->orderCheck->getOrder($params['orderId'], $params['id']);
    if (!$order) {
      $this->logger->info('Order not found');
      return FALSE;
    }
    $status = FALSE;

    try {
      $status = $this->request->checkPurchase($order);
      if ($order->getState() == 'new') {
        if ($status == 'SUCCESS') {
          $this->orderCheck->processOrder($order);
        }
        elseif ($status == 'REJECTED') {
          $this->orderCheck->cancelOrder($order);
        }
        elseif ($status == 'FAILED') {
          $this->orderCheck->cancelOrder($order);
        }
      }
    } catch (\Exception $e) {
      $this->logger->info('Exception when processing order');
    }
  }

}
