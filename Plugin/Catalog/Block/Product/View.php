<?php

namespace Webexpert\Slice\Plugin\Catalog\Block\Product;

use Magento\Catalog\Block\Product\View as ProductView;
use Magento\Catalog\Helper\Data;
use Magento\Store\Model\ScopeInterface;

class View {

  private $helper;
  private $_product;
  private $priceHelper;
  private $displayBlocks = ['product.info.addtocart'];
  public $scopeConfig;

  public function __construct(
    Data                                               $helper,
    \Magento\Framework\Pricing\Helper\Data             $priceHelper,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
  ) {
    $this->helper = $helper;
    $this->priceHelper = $priceHelper;
    $this->scopeConfig = $scopeConfig;
  }

  public function afterToHtml(ProductView $subject, $html) {
    if (in_array($subject->getNameInLayout(), $this->displayBlocks)) {
      if ($this->scopeConfig->getValue('payment/slice/product_view_monthly', ScopeInterface::SCOPE_STORE)) {
        $price = $this->getMonthlyPayment();
        if ($price) {
          return $html . '<div>' . '<strong>' . __('3 payments %1 month, without extra fees. ', $this->getMonthlyPayment()) . '</strong>' . $this->getImage() . '</div>';
        }
      }
    }
    return $html;
  }

  private function getImage() {
    $image = $this->scopeConfig->getValue('payment/slice/image_remote', ScopeInterface::SCOPE_STORE);
    return "<img src=" . $image . " alt='Modena' style='max-width: 70px; transform: translate(0%, 15%);'>";
  }

  //this one will get product specific estimate as opposed to quote specific
  private function getMonthlyPayment() {
    $product = $this->getProduct();
    if ($product) {
      $price = $product->getFinalPrice();
      if ($price > 0) {
        return $this->priceHelper->currency($price / 3, TRUE, FALSE);
      }
    }
    return FALSE;
  }

  private function getProduct() {
    if (is_null($this->_product)) {
      $this->_product = $this->helper->getProduct();
    }
    return $this->_product;
  }

}

