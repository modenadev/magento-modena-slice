<?php

namespace Webexpert\Slice\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

class ConfigProvider implements ConfigProviderInterface {

  const SLICE_CODE = 'slice';

  protected $urlInterface;

  private $storeManager;

  private $scopeConfig;

  private $checkoutSession;

  private $priceHelper;

  private $quoteFactory;

  protected $localeCurrency;

  public function __construct(
    \Magento\Framework\UrlInterface        $urlInterface,
    StoreManagerInterface                  $storeManager,
    ScopeConfigInterface                   $scopeConfig,
    \Magento\Checkout\Model\Session        $checkoutSession,
    \Magento\Framework\Pricing\Helper\Data $priceHelper,
    \Magento\Quote\Model\QuoteFactory      $quoteFactory,
    \Magento\Framework\Locale\CurrencyInterface $localeCurrency
  ) {
    $this->urlInterface = $urlInterface;
    $this->storeManager = $storeManager;
    $this->scopeConfig = $scopeConfig;
    $this->checkoutSession = $checkoutSession;
    $this->priceHelper = $priceHelper;
    $this->quoteFactory = $quoteFactory;
    $this->localeCurrency = $localeCurrency;
  }

  public function getConfig() {
    return [
      'payment' => [
        self::SLICE_CODE => [
          'redirect_url' => $this->urlInterface->getUrl('slice/request/redirect'),
          'code' => self::SLICE_CODE,
          'display_logo' => $this->scopeConfig->getValue('payment/slice/display_logo', ScopeInterface::SCOPE_STORE),
          'logo' => $this->getLogoUrl(),
          'monthly_payment' => $this->getMonthlyPayment(),
          'oneliner'   => $this->scopeConfig->getValue('payment/slice/oneliner', ScopeInterface::SCOPE_STORE),
          'description' => $this->scopeConfig->getValue('payment/slice/description', ScopeInterface::SCOPE_STORE),
        ],
      ],
    ];
  }

  private function getLogoUrl() {
    $image = $this->scopeConfig->getValue('payment/slice/image_remote', ScopeInterface::SCOPE_STORE);

    if ($image) {
      return $image;
    }
    else {
      return FALSE;
    }
  }

  private function getMonthlyPayment() {
    $quote = $this->checkoutSession->getQuote();
    if ($quote) {
      $grandTotal = $quote->getGrandTotal();
      return round($grandTotal / 3) . ' ' . $this->localeCurrency->getCurrency($this->storeManager->getStore()->getCurrentCurrencyCode())->getSymbol();
    }
    else {
      return FALSE;
    }
  }

}
