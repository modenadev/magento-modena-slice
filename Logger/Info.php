<?php

namespace Webexpert\Slice\Logger;

class Info extends \Magento\Framework\Logger\Handler\Base {

  protected $loggerType = \Monolog\Logger::INFO;

  protected $level = \Monolog\Logger::INFO;

  protected $fileName = '/var/log/webexpert_slice.log';

  public function isHandling(array $record): bool {
    return $record['level'] === $this->level;
  }

}
