/**
 * Copyright ©  Webexpert Group. All rights reserved.
 * See LICENSE.txt for license details.
 */
define(
  [
    'jquery',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/action/place-order',
    'Webexpert_Slice/js/action/set-payment-method',
    'mage/translate',
    'Magento_Checkout/js/model/quote'
  ],
  function ($, Component, placeOrderAction, setPaymentMethod, additionalValidators, $translate, quote) {
    'use strict';
    return Component.extend({
      redirectAfterPlaceOrder: false,
      displayLogo: window.checkoutConfig.payment.slice.display_logo,
      defaults: {
        template: 'Webexpert_Slice/payment/slice',
      },
      getCode: function () {
        return window.checkoutConfig.payment.slice.code;
      },
      getPaymentIcon: function () {
        return window.checkoutConfig.payment.slice.logo;
      },
      getMonthlyPayment: function () {
        return window.checkoutConfig.payment.slice.monthly_payment;
      },
      getOneliner: function () {
        return window.checkoutConfig.payment.slice.oneliner;
      },
      getDescription: function () {
        return window.checkoutConfig.payment.slice.description;
      },
    getData: function () {
        return {
          'method': this.item.method,
          'additional_data': {}
        };
      },
      afterPlaceOrder: function () {
        setPaymentMethod();
      },
    });
  }
);
