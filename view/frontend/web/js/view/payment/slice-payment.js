define(
  [
    'uiComponent',
    'Magento_Checkout/js/model/payment/renderer-list'
  ],
  function (
    Component,
    rendererList
  ) {
    'use strict';
    rendererList.push(
      {
        type: 'slice',
        component: 'Webexpert_Slice/js/view/payment/method-renderer/slice-method'
      }
    );
    return Component.extend({});
  }
);
