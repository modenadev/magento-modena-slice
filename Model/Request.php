<?php

namespace Webexpert\Slice\Model;

use Laminas\Http\Request as LaminasRequest;
use Magento\Sales\Api\Data\OrderInterface;
use Webexpert\Slice\Model\Adminhtml\Source\Mode;

class Request {

  public $scopeConfig;

  public $authToken;

  public $logger;

  public $url;

  public $mode;

  public $curlClient;

  public function __construct(
    \Magento\Framework\HTTP\LaminasClientFactory       $httpClientFactory,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Framework\UrlInterface                    $url,
    \Webexpert\Slice\Logger\Logger                     $logger,
    \Webexpert\Slice\Model\Adminhtml\Source\Mode       $mode,
    \Magento\Framework\HTTP\Client\Curl                $curl

  ) {
    $this->scopeConfig = $scopeConfig;
    $this->url = $url;
    $this->logger = $logger;
    $this->mode = $mode;
    $this->curlClient = $curl;
  }

  public function getClient() {
    $client = new \Laminas\Http\Client();
    $client->setMethod(LaminasRequest::METHOD_POST);
    $client->setHeaders(['Content-Type' => 'application/x-www-form-urlencoded']);
    return $client;
  }

  public function authenticate() {
    $data = [
      'grant_type' => 'client_credentials',
      'scope' => Mode::SCOPE,
    ];
    $client = $this->getClient();
    $client->setAuth($this->mode->getStoreId(), $this->mode->getSecret(), 'basic');
    $client->setUri(sprintf('%s/oauth2/token', $this->mode->getSliceUrl()));
    $client->setParameterPost($data);

    $response = $client->send();

    if ($response->isClientError() || $response->isServerError()) {
      $this->logger->info($response);
      throw new \Exception('Authentication with api failed');
    }

    $responseData = json_decode($response->getBody(), TRUE);
    $this->authToken = $responseData['access_token'];

    return $this;
  }

  public function initPurchase(OrderInterface $order) {
    $result = ''; 
    try {
      $this->authenticate();
      $send = $this->setSend($order);
      $curl = $this->setCurl();
      $curl->post(sprintf('%s/%s', $this->mode->getSliceRedirect(), Mode::PATH), json_encode($send));
      if (array_key_exists('location', $curl->getHeaders())) {
        //TODO: figure out how to get this more elegantly
        $lines = explode("\n", $curl->getBody());
        $lines = array_slice($lines, -1);
        $result = json_decode(implode("\n", $lines), TRUE);
        $result['location'] = $curl->getHeaders()['location'];
        return $result;
      }
      else {
        $this->logger->info($result);
        throw new \Exception('Error when creating order');
      }

    } catch (\Exception $e) {
      $this->logger->info($e->getMessage());
      throw $e;
    }
  }

  private function setSend($order) {
    $send = [
      'maturityInMonths' => Mode::MATURITYINMONTHS,

      "orderId" => $order->getIncrementId(),
      "totalAmount" => $order->getGrandTotal(),
      "currency" => $order->getOrderCurrencyCode(),
      'orderItems' => [],
      'customer' =>
        [
          "email" => $order->getCustomerEmail(),
          "phoneNumber" => $order->getShippingAddress()->getTelephone(),
          "address" => $order->getShippingAddress()
              ->getStreet()[0] . ', ' . $order->getShippingAddress()
              ->getCity() . ', ' . $order->getShippingAddress()->getPostcode(),
        ],
      "timestamp" => str_replace(" ", "T", $order->getCreatedAt()) . 'Z',
      "returnUrl" => $this->url->getUrl('slice/response/success'),
      "callbackUrl" => $this->url->getUrl('slice/response/callback'),
      "cancelUrl" =>  $this->url->getUrl('slice/response/cancel'),
    ];
    $items = $order->getAllVisibleItems();
    foreach ($items as $item) {
      $send['orderItems'][] = [
        'description' => $item->getName(),
        'amount' => (string) $item->getRowTotalInclTax(),
        'currency' => $order->getOrderCurrencyCode(),
        'quantity' => round($item->getQtyOrdered()),
      ];
    }
    $send['orderItems'][] = [
      'description' => $order->getShippingDescription(),
      'amount' => $order->getShippingInclTax(),
      'currency' => $order->getOrderCurrencyCode(),
      'quantity' => '1',
    ];
    if($order->getDiscountAmount() != '0.0000'){
      $send['orderItems'][] = [
        'description' => 'Sooduskood',
        'amount' => $order->getDiscountAmount(),
        'currency' => $order->getOrderCurrencyCode(),
        'quantity' => '1',
      ];
    }
    return $send;
  }

  private function setCurl() {
    //using a seprate curl solution because the other one didnt seem to use some of the options we need
    $curl = $this->curlClient;
    $curl->setHeaders(
      [
        'Authorization' => 'Bearer ' . $this->authToken,
        'Content-Type' => 'application/json',
      ]
    );
    $curl->setOption(CURLOPT_SSL_VERIFYPEER, 0);
    $curl->setOption(CURLOPT_SSL_VERIFYHOST, 0);
    $curl->setOption(CURLOPT_URL, sprintf('%s/%s', $this->mode->getSliceRedirect(), Mode::PATH));
    $curl->setOption(CURLOPT_RETURNTRANSFER, 1);
    $curl->setOption(CURLOPT_HEADER, 1);
    $curl->setOption(CURLOPT_POST, 1);
    $curl->setOption(CURLOPT_FOLLOWLOCATION, FALSE);
    return $curl;

  }

  public function checkPurchase(OrderInterface $order) {

    $this->authenticate();
    $purchaseId = $order->getPayment()->getData()['slice_id'];
    if (!$purchaseId) {
      return FALSE;
    }
    try {
      $client = $this->getClient();
      $client->setUri(
        sprintf('%s/modena/api/merchant/applications/%s/status', $this->mode->getSliceRedirect(), $purchaseId)
      );
      $client->setHeaders(['Authorization' => sprintf('Bearer %s', $this->authToken)]);
      $client->setMethod(LaminasRequest::METHOD_GET);
      $response = $client->send();
      if ($response->isClientError() || $response->isServerError()) {
        throw new \Exception('Error when checking pending order status');
      }
      if ($response) {
        return json_decode($response->getBody(), TRUE)['status'];
      }
      else {
        return FALSE;
      }
    } catch (\Exception $e) {

      $this->logger->info($e->getMessage());
      throw $e;
    }

    return $responseData;
  }

}
