<?php

namespace Webexpert\Slice\Model\Adminhtml\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Store\Model\ScopeInterface;

class Mode implements ArrayInterface {

  const MATURITYINMONTHS = '3';

  const SCOPE = 'slicepayment';

  const PATH = 'modena/api/merchant/slice-payment-order';

  public $scopeConfig;

  public function __construct(
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
  ) {
    $this->scopeConfig = $scopeConfig;
  }

  public function toOptionArray() {
    return [
      [
        'value' => 'test',
        'label' => 'Test',
      ],
      [
        'value' => 'production',
        'label' => 'Production',
      ],
    ];
  }

  public function getSliceUrl() {
    if ($this->scopeConfig->getValue('payment/slice/mode', ScopeInterface::SCOPE_STORE) == 'production') {
      return $this->scopeConfig->getValue('payment/slice/production_payment_url', ScopeInterface::SCOPE_STORE);
    }
    else {
      if ($this->scopeConfig->getValue('payment/slice/mode', ScopeInterface::SCOPE_STORE) == 'test') {
        return $this->scopeConfig->getValue('payment/slice/test_payment_url', ScopeInterface::SCOPE_STORE);
      }
    }
    return NULL;
  }

  public function getSliceRedirect() {
    if ($this->scopeConfig->getValue('payment/slice/mode', ScopeInterface::SCOPE_STORE) == 'production') {
      return $this->scopeConfig->getValue('payment/slice/production_redirect_url', ScopeInterface::SCOPE_STORE);
    }
    else {
      if ($this->scopeConfig->getValue('payment/slice/mode', ScopeInterface::SCOPE_STORE) == 'test') {
        return $this->scopeConfig->getValue('payment/slice/test_redirect_url', ScopeInterface::SCOPE_STORE);

      }
    }
    return NULL;
  }
  public function getStoreId() {
    if ($this->scopeConfig->getValue('payment/slice/mode', ScopeInterface::SCOPE_STORE) == 'production') {
      return $this->scopeConfig->getValue('payment/slice/store_id', ScopeInterface::SCOPE_STORE);
    }
    else {
      if ($this->scopeConfig->getValue('payment/slice/mode', ScopeInterface::SCOPE_STORE) == 'test') {
        return $this->scopeConfig->getValue('payment/slice/test_store_id', ScopeInterface::SCOPE_STORE);

      }
    }
    return NULL;
  }
  public function getSecret() {
    if ($this->scopeConfig->getValue('payment/slice/mode', ScopeInterface::SCOPE_STORE) == 'production') {
      return $this->scopeConfig->getValue('payment/slice/secret_key', ScopeInterface::SCOPE_STORE);
    }
    else {
      if ($this->scopeConfig->getValue('payment/slice/mode', ScopeInterface::SCOPE_STORE) == 'test') {
        return $this->scopeConfig->getValue('payment/slice/test_secret_key', ScopeInterface::SCOPE_STORE);
      }
    }
    return NULL;
  }
}

