<?php

namespace Webexpert\Slice\Controller\Response;

use Webexpert\Slice\Model\ConfigProvider;
use Magento\Store\Model\ScopeInterface;

class Cancel extends \Magento\Framework\App\Action\Action {

  protected $logger;

  protected $request;

  protected $orderCheck;

  protected $checkoutSession;

  protected $orderFactory;

  protected $quoteFactory;

  private $quoteRepository;

  protected $successValidator;

  private $scopeConfig;

  public function __construct(
    \Magento\Framework\App\Action\Context $context,
    \Magento\Checkout\Model\Session       $session,
    \Magento\Sales\Model\OrderFactory     $orderFactory,
    \Psr\Log\LoggerInterface              $logger,
    \Webexpert\Slice\Model\Request        $request,
    \Webexpert\Slice\Model\OrderCheck     $orderCheck,
    \Magento\Quote\Model\QuoteFactory     $quoteFactory,
    \Magento\Quote\Model\QuoteRepository $quoteRepository,
    \Magento\Checkout\Model\Session\SuccessValidator $successValidator,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
  ) {
    parent::__construct($context);
    $this->logger = $logger;
    $this->request = $request;
    $this->orderCheck = $orderCheck;
    $this->checkoutSession = $session;
    $this->orderFactory = $orderFactory;
    $this->quoteFactory = $quoteFactory;
    $this->quoteRepository = $quoteRepository;
    $this->successValidator = $successValidator;
    $this->scopeConfig = $scopeConfig;
  }

  public function execute() {

    $params = filter_input_array(INPUT_POST);
    if (!$params) {
      $this->errorPage();
    }
    $order = $this->orderCheck->getOrder($params['orderId'], $params['id']);
    if (!$order) {
      $this->errorPage();
    }
    $quote = $this->quoteFactory->create()->load($order->getQuoteId());
    try {
      if ($order->getState() == 'new') {
        $this->orderCheck->cancelOrder($order);

        if ($quote->getPayment()
          ->getMethodInstance()
          ->getCode() == ConfigProvider::SLICE_CODE &&  $this->scopeConfig->getValue('payment/slice/restore_quote', ScopeInterface::SCOPE_STORE)) {
          $this->checkBeforeCheckout($order, $quote);
        }else{
          $this->messageManager->addErrorMessage(__('Payment has been rejected.'));
        }
        return $this->_redirect('checkout/');
      }else{
        //this should not happen
        $this->errorPage();
      }
    } catch (\Exception $e) {
      $this->errorPage();
    }
  }

  private function errorPage() {
    $this->messageManager->addErrorMessage(__('Could not initiate payment due to wrong module setup. Please contact the store staff.'));
    return $this->_redirect('/');
  }

  public function checkBeforeCheckout($order, $quote) {
    if(!$this->successValidator->isValid()) {
      $quote->setIsActive(1)->setReservedOrderId(NULL);
      $this->quoteRepository->save($quote);
      $this->checkoutSession->replaceQuote($quote);
    }
  }
}

